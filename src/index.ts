import { CloudWatch, DynamoDB } from 'aws-sdk'
import Rchain from 'ramda/src/chain'
import Rconcat from 'ramda/src/concat'
import Rdivide from 'ramda/src/divide'
import Rfilter from 'ramda/src/filter'
import Rflip from 'ramda/src/flip'
import Rmax from 'ramda/src/max'
import Rpipe from 'ramda/src/pipe'
import Rprop from 'ramda/src/prop'
import RpropEq from 'ramda/src/propEq'
import Rreduce from 'ramda/src/reduce'
import Rsum from 'ramda/src/sum'
// import Rtap from 'ramda/src/tap'

export interface DdbCostOptimizeOptions {
  readonly TableName: string
  readonly cw?: CloudWatch
  readonly ddb?: DynamoDB
  readonly StartTime: Date
  readonly EndTime?: Date
}
interface DdbCostOptimizeOptionsPopulated {
  readonly TableName: string
  readonly cw: CloudWatch
  readonly ddb: DynamoDB
  readonly StartTime: Date
  readonly EndTime: Date
}
interface GetMetricsOptions {
  readonly TableName: string
  readonly StartTime: Date
  readonly EndTime: Date
}
export interface DdbCostOptimized {
  readonly currentMode: string
  readonly onDemandCost: any
  readonly provisionedCost: any
  // reserved
  // auto-scaling
}

const
  cost = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' }),
  divideBy = Rflip(Rdivide),
  addDefaults = ({ cw, ddb, EndTime, ...rest }: DdbCostOptimizeOptions): Promise<DdbCostOptimizeOptionsPopulated> =>
    Promise.resolve({
      cw: cw ?? new CloudWatch(),
      ddb: ddb ?? new DynamoDB(),
      EndTime: EndTime ?? new Date(),
      ...rest,
    }),
  getTableInfo = (ddb: DynamoDB, TableName: string) =>
    ddb.describeTable({ TableName }).promise()
      .then(Rprop<string, any>('Table')),
  getMetrics = (cw: CloudWatch, { StartTime, EndTime, TableName }: GetMetricsOptions, NextToken?: string): Promise<CloudWatch.Types.MetricDataResults> =>
    cw.getMetricData({
      MetricDataQueries: [
        {
          Id: 'read',
          MetricStat: {
            Period: 300,
            Stat: 'Sum',
            Metric: {
              MetricName: 'ConsumedReadCapacityUnits',
              Namespace: 'AWS/DynamoDB',
              Dimensions: [ {
                Name: 'TableName',
                Value: TableName,
              } ],
            },
          },
        },
        {
          Id: 'write',
          MetricStat: {
            Period: 300,
            Stat: 'Sum',
            Metric: {
              MetricName: 'ConsumedWriteCapacityUnits',
              Namespace: 'AWS/DynamoDB',
              Dimensions: [ {
                Name: 'TableName',
                Value: TableName,
              } ],
            },
          },
        },
      ],
      StartTime,
      EndTime,
      NextToken,
    }).promise()
      .then(({ MetricDataResults = [], NextToken }) =>
        !NextToken
          ? MetricDataResults
          : getMetrics(cw, { TableName, StartTime, EndTime }, NextToken).then(Rconcat(MetricDataResults)))

export const getReqs = Rpipe<CloudWatch.Types.MetricDataResults, number[], number>(
  Rchain(Rprop<string, any>('Values')),
  Rsum
)
export const getMillionReqs = Rpipe<CloudWatch.Types.MetricDataResults, number, number, number>(
  getReqs,
  divideBy(1000000),
  Math.ceil
)
export const calcOnDemand = (metrics: CloudWatch.Types.MetricDataResults) => {
  const
    millionReads = getMillionReqs(Rfilter(RpropEq('Id', 'read'), metrics as any)),
    millionWrites = getMillionReqs(Rfilter(RpropEq('Id', 'write'), metrics as any)),
    readCost = millionReads * 0.283,
    writeCost = millionWrites * 1.4135,
    sum = readCost + writeCost
  return {
    millionReads,
    millionWrites,
    readCost: cost.format(readCost),
    writeCost: cost.format(writeCost),
    sum: cost.format(sum),
  }
}
export const getTopReq = Rpipe<CloudWatch.Types.MetricDataResults, number[], number, number, number>(
  Rchain(Rprop<string, any>('Values')),
  Rreduce<number, number>(Rmax, 0),
  divideBy(60 * 5), // capacity units are per second, so convert from 5 min to 1 second
  Math.ceil
)
export const calcProvisioned = (timespanHours: number) => (metrics: CloudWatch.Types.MetricDataResults) => {
  const
    RCU = getTopReq(Rfilter(RpropEq('Id', 'read'), metrics as any)),
    WCU = getTopReq(Rfilter(RpropEq('Id', 'write'), metrics as any)),
    readCost = RCU * 0.000147 * timespanHours,
    writeCost = WCU * 0.000735 * timespanHours,
    sum = readCost + writeCost
  return {
    RCU,
    WCU,
    readCost: cost.format(readCost),
    writeCost: cost.format(writeCost),
    sum: cost.format(sum),
  }
}
export const isSpiky = Rpipe<CloudWatch.Types.MetricDataResults, number[], any, boolean>(
  Rchain(Rprop<string, any>('Values')),
  Rreduce(
    ({ lastValue, spiky }, consumed: number) =>
      spiky ? ({ lastValue, spiky })
      : (lastValue >= 10 && consumed > lastValue * 2) ? ({ lastValue, spiky: true })
      : ({ lastValue: consumed, spiky: false }),
    { lastValue: 0, spiky: false }
  ),
  Rprop('spiky')
)
export const calcAutoscaling = (timespanHours: number) => (metrics: CloudWatch.Types.MetricDataResults) => {
  const
    // get requests per second, target 70% utilization
    RCU = Math.ceil((getReqs(Rfilter(RpropEq('Id', 'read'), metrics as any)) / (timespanHours * 60 * 60)) / 0.7),
    WCU = Math.ceil((getReqs(Rfilter(RpropEq('Id', 'write'), metrics as any)) / (timespanHours * 60 * 60)) / 0.7),
    spikyRead = isSpiky(Rfilter(RpropEq('Id', 'read'), metrics as any)),
    spikyWrite = isSpiky(Rfilter(RpropEq('Id', 'write'), metrics as any)),
    readCost = RCU * 0.000147 * timespanHours,
    writeCost = WCU * 0.000735 * timespanHours,
    sum = readCost + writeCost
  return {
    RCU,
    WCU,
    spikyRead,
    spikyWrite,
    readCost: cost.format(readCost),
    writeCost: cost.format(writeCost),
    sum: cost.format(sum),
  }
}

export const ddbCostOptimize = (opts: DdbCostOptimizeOptions): Promise<DdbCostOptimized> =>
  addDefaults(opts)
    .then(({ cw, ddb, TableName, StartTime, EndTime }: DdbCostOptimizeOptionsPopulated) =>
      Promise.all([
        getTableInfo(ddb, TableName),
        getMetrics(cw, { TableName, StartTime, EndTime }),
      ])
        .then(([ table, metrics ]: [ DynamoDB.Types.TableDescription, CloudWatch.Types.MetricDataResults ]) => ({
          tableName: table.TableName!,
          currentMode: table.BillingModeSummary!.BillingMode!,
          onDemandCost: calcOnDemand(metrics),
          provisionedCost: calcProvisioned((EndTime.valueOf() - StartTime.valueOf()) / (1000 * 60 * 60))(metrics),
          autoscalingCost: calcAutoscaling((EndTime.valueOf() - StartTime.valueOf()) / (1000 * 60 * 60))(metrics),
          // reserved
        }))
    )
