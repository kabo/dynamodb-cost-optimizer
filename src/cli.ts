// import Rtap from 'ramda/src/tap'
import yargs from 'yargs'
// @ts-ignore
import { hideBin } from 'yargs/helpers'
import { ddbCostOptimize } from './index'

interface Argv {
  readonly tableName: string
  readonly startTime: string
  readonly endTime?: string
}
const
  getArgs = (argv: any): Promise<Argv> =>
    Promise.resolve(yargs(hideBin(argv))
      .option('t', {
        alias: 'tableName',
        type: 'string',
        demandOption: true,
        describe: 'DynamoDB table to optimize',
      })
      .option('s', {
        alias: 'startTime',
        type: 'string',
        demandOption: true,
        describe: 'Consider metrics from this time',
      })
      .option('e', {
        alias: 'endTime',
        type: 'string',
        describe: 'Consider metrics up to this time',
      })
      .help()
      .argv as any),
  stringify = (sgs: any): string => JSON.stringify(sgs, null, 2) // eslint-disable-line fp/no-nil

// eslint-disable-next-line fp/no-unused-expression
getArgs(process.argv)
  .then(({ tableName, startTime, endTime }: Argv) =>
    ddbCostOptimize({
      TableName: tableName,
      StartTime: new Date(startTime),
      EndTime: endTime ? new Date(endTime) : new Date(),
    })
  )
  // .then(Rtap(console.log))
  .then(stringify)
  .then(console.log)
  .catch(console.error)
