# DynamoDB Cost Optimizer

> Optimize your DynamoDB tables based on past usage

## Install

Install globally if you want to use the CLI.

```sh
yarn global add dynamodb-cost-optimizer
# or
npm i -g dynamodb-cost-optimizer
```

Install locally if you want to use it as a library.

```sh
yarn add dynamodb-cost-optimizer
# or
npm i --save dynamodb-cost-optimizer
```

## Usage

The utility estimates how much the table would have cost using different setups. You still need to take into account if your usage is varied, if it's predictable, and if it's spiky. Also keep in mind that past usage patterns is no guarantee for future usage patterns. This utility is just a coarse guide. If you blindly configure your tables to use the cheapest option you're gonna have a bad time.

### CLI

```sh
$ dynamodb-cost-optimizer --help
Options:
      --version    Show version number                                 [boolean]
  -t, --tableName  DynamoDB table to optimize                [string] [required]
  -s, --startTime  Consider metrics from this time           [string] [required]
  -e, --endTime    Consider metrics up to this time                     [string]
      --help       Show help                                           [boolean]
$ dynamodb-cost-optimizer -t MyTable -s 2021-04-07T00:00:00
{
  "tableName": "MyTable",
  "currentMode": "PAY_PER_REQUEST",
  "onDemandCost": {
    "millionReads": 1,
    "millionWrites": 1,
    "readCost": "$0.28",
    "writeCost": "$1.41",
    "sum": "$1.70"
  },
  "provisionedCost": {
    "RCU": 190,
    "WCU": 1,
    "readCost": "$4.62",
    "writeCost": "$0.12",
    "sum": "$4.74"
  },
  "autoscalingCost": {
    "RCU": 2,
    "WCU": 1,
    "spikyRead": false,
    "spikyWrite": true,
    "readCost": "$0.05",
    "writeCost": "$0.12",
    "sum": "$0.17"
  }
}
```

### As a library

```typescript
import { ddbCostOptimize } from 'dynamodb-cost-optimizer'

ddbCostOptimize({ TableName: 'MyTable', StartTime: new Date('2021-04-07T00:00:00') })
  .then(console.log)
```

## Output

### onDemandCost

millionReads: How many million reads were found during the period.

millionWrites: How many million writes were found during the period.

readCost: How much the above reads would cost.

writeCost: How much the above writes would cost.

sum: readCost + writeCost.

### provisionedCost

RCU: The max consumed read units found during the period.

WCU: The max consumed write units found during the period.

readCost: How much the above read capacity would cost if provisioned.

writeCost: How much the above write capacity would cost if provisioned.

sum: readCost + writeCost.

### autoscalingCost

RCU: The average RCU needed to keep the consumed read units found during the period at 70% utilization.

WCU: The average WCU needed to keep the consumed write units found during the period at 70% utilization.

spikyRead: true if consumed reads more than double over a 5 min period. This could indicate that the table is unsuitable to use autoscaling.

spikyWrite: true if consumed writes more than double over a 5 min period. This could indicate that the table is unsuitable to use autoscaling.

readCost: How much the above read capacity would cost if provisioned.

writeCost: How much the above write capacity would cost if provisioned.

sum: readCost + writeCost.

## API

### TableName

Type: `string`\
Required: `yes`

The DynamoDB table to analyze.

### StartTime

Type: `Date`\
Required: `yes`

Consider metrics from this time.

### EndTime

Type: `Date`\
Default: `new Date()`

Consider metrics up to this time.

### cw

Type: `AWS.CloudWatch`\
Default: `new CloudWatch()`

The AWS CloudWatch client to use for CloudWatch API calls.

### ddb

Type: `AWS.DynamoDB`\
Default: `new DynamoDB()`

The AWS DynamoDB client to use for DynamoDB API calls.

## Developing

1. Clone the repo
1. Run `yarn install`
1. Run `yarn test-watch` to run the tests while deving
1. Run `git add . && yarn cm` to commit changes using [commitizen](https://github.com/commitizen/cz-cli)
1. Run `yarn release` to create a new version using [standard-version](https://github.com/conventional-changelog/standard-version)

Lint checks and tests are run automatically on commit and built by the pipeline on push.

## License

dynamodb-cost-optimizer is licensed under the terms of the MIT license.
