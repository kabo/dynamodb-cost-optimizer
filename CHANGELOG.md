# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.1 (2021-04-07)


### Features

* first commit ([7f80926](https://gitlab.com/kabo/dynamodb-cost-optimizer/commit/7f80926348a762742ef2d95e72abc63af3ca3dc3))
