/* global describe, expect, it, jest */
/* eslint-disable better/explicit-return, fp/no-unused-expression, fp/no-nil */
import { ddbCostOptimize } from '../src/index'

describe('index', () => {
  describe('ddbCostOptimize', () => {
    it('is a function', () => {
      expect(typeof ddbCostOptimize).toBe('function')
    })
  })
})
